---
title: "Strawberry Dark Chocolate Muffins"
description: "tempting and healthy"
slug: "muffin2"
image: strawberry_dark_choc_muffin_1.jpg
keywords: ""
categories: 
    - ""
    - ""
date: 2017-10-31T22:26:09-05:00
draft: false
---

### Ingredients:

2 cup rolled oats
1/2 cup flour of your choice 
*(eg. protein powder or almond flour, coconut flour, oat flour or regular flour)*
2 ***super ripe*** bananas
2 eggs 
*(Can be left out or replaced with 1/2 cup non-fat greek yogurt or soaked chia seeds for vegetarian/vegan versions respectively)* 
1 cup milk of your choice 
*(eg. regular milk, soy milk or almond milk)*
1 tsp. vanilla
1 tsp. baking powder
1/2 tsp. salt
1/2 tbsp. lemon or orange zest
1/2 cup fresh blueberries
1/2 cup seed mix of your choice 
*(I use a mix of sunflower and pumpkin seeds)*
4-6 large medjul dates 
*(more dates can be used to increase the sweetness as per your requirement)*
Extra Topping: A small piece of your favourite chocolate.


### Instructions:
1. Preheat oven to 180 deg C or 350 deg F. 
2. Place the bananas in the oven during this time for around 5-10 mins OR till they are completely brown.
3. Chop the dates so that they can be divided equally into the 12 muffins. * I use large medjool dates and cut each into four or six parts.*
4. Prepare the muffin trays. Grease or line a 12-cup muffin tray with muffin cup liners.*I use a silicon muffin tray so it doesnot require any coating with oil or butter.* 
5. In a bowl mash the bananas and combine with eggs, vanilla, milk and salt.
6. Mix the dry ingredients rolled oats, flour, baking powder seperately and combine with the wet indregient mix from **step 5**. Finally add the lemon or orange zest to the mixture. ***Donot overmix the batter***
7. Put half of a serving (one spoon of the batter) into each of the 12 muffin slots. Add equal amount of seed mix, dates and blueberries to each cup. Divide the remaining batter evenly among the muffin cups. * Optional : Top each muffin with a **small** piece of your favourite chocolate *
8. Bake for 22-27 minutes or till a knife comes out clean. Cool on a rack and enjoy !

### Storage :
* 3-4 days in an airtight container
* Can be frozen for upto 3 months : Completely cool the muffins at room temperature and place seperately in the freezer for an hour. This will prevent them from sicking together or getting smashed. Pack them in a bag or an airtight container and keep frozen. They can be thawed at room temperature (30 minutes) or microwaved (30 seconds) or wrappedin a foil and heated in the oven (10 mins, 180 deg C) 