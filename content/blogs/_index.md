---
title: "Healthy muffins"
slug: "blogs"
image: pic02.jpg
date: 2017-10-31T22:27:21-05:00
draft: false
---

These easy to make sumptous versatile muffins are free of refined flours, sugar, oil and butter. I play with ingredients and use medjul dates and ripe fruits to satisfy the sweet tooth. It just cannot get healthier and easier to bring a big smile on your face and have your loved ones beaming while enjoying these grab-and-go breakfast or lazy afternoon snack bites. With minor variations to the basic recipe unleash the creativity within you to mix and match the add-ons. For some extra magic, add A SMALL piece of your favourite chocolate in the centre to create a melted lava effect.